# Closure

 Відрефакторити код позбавившись від дублювання коду
 
 Для цього створити функцію createZoom, яка буде приймати назву селектора
 (selector) та значення зуму (zoomValue)
 
 ADVANCED:
 Після створення зуму дати можливість викликати цю функцію, щоб дізнатися її
 параметри та елемент
 
 Наприклад, 
 const zoomOut = createZoom('#zoom-out', 0.8);
 console.log(zoomOut()) // { zoom: 0.8,  }
 