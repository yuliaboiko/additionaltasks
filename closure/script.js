// Установка размера текста
function setFontSize(zoom) {
    const fontSize = getComputedStyle(document.body).fontSize;
    const fontSizeValue = parseInt(fontSize);
    document.body.style.fontSize = `${fontSizeValue * zoom}px`;
}

// Создание зума
function createZoom(selector, zoomValue) {
    const zoomEl = document.querySelector(`${selector}`);

    zoomEl.addEventListener('click', function () {
        setFontSize(zoomValue);
    })

    return function () {
        return {
            zoom: zoomValue,
        }
    }

}

// Создание новой кнопки
function createZoomBtn(zoomValue) {
    const zoomEl = document.createElement('button');
    zoomEl.classList.add('counter');
    zoomEl.innerText = `x${zoomValue}`;
    zoomEl.id = createZoomId(zoomValue);

    counterBlockEl.append(zoomEl);
    return zoomEl.id;
}

// Создание id для новой кнопки
function createZoomId(zoomValue) {
    let validId = '';
    zoomValue.includes('.') ?
        validId = zoomValue.replace('.', '-') :
        validId = zoomValue;

    return zoomValue > 1 ? `zoom-in-${validId}` : `zoom-out-${validId}`;
}


const zoomMap = ['0.8', '1', '2'];

// Дом элементы для добавления новых кнопок
const counterBlockEl = document.querySelector('.counter-block');
const addZoomEl = document.querySelector('.zoom-btn');
const addZoomInputEl = document.querySelector('.zoom-input');

addZoomEl.addEventListener('click', function () {
    let value = addZoomInputEl.value;

    if (value < 0) {
        alert('Введите положительное число');
        addZoomInputEl.value = '';
        return;
    }

    if (zoomMap.includes(value)) {
        alert(`Зум со значением ${value} уже существует. Введите другое значение`);

    } else {
        let selector = `#${createZoomBtn(value)}`;
        createZoom(selector, value);
        zoomMap.push(value);
    }
    addZoomInputEl.value = '';
})



const zoomOut = createZoom('#zoom-out', 0.8);
// console.log(zoomOut());

const zoomIn1 = createZoom('#zoom-in-1', 1.5);
// console.log(zoomIn1());

const zoomIn2 = createZoom('#zoom-in-2', 2);
// console.log(zoomIn2());