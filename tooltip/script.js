class Tooltip {
    constructor(targetSelector, text, placement = 'top') {
        this.targetElement = document.querySelector(targetSelector);
        this.isDisabled = false;
        this.targetElement.classList.add('tooltip');
        this.placement = placement;
        this.text = text;
        this.render();
        this.classMap = {
            top: 'tooltip-up',
            right: 'tooltip-right',
            bottom: 'tooltip-down',
            left: 'tooltip-left',
        }
    }

    getPositionClassName() {
        return this.classMap[this.placement];
    }

    changeText(newText) {
        this.tooltip.textContent = newText;
    }

    setPosition(position) {
        const posArr = Object.keys(this.classMap);

        if (!posArr.includes(position)) return;

        this.targetElement.classList.remove(this.getPositionClassName());
        this.placement = position;
        this.targetElement.classList.add(this.getPositionClassName());
    }

    render() {
        this.tooltip = document.createElement('span');
        this.tooltip.classList.add('tooltip-content');
        this.tooltip.textContent = this.text;
        this.targetElement.prepend(this.tooltip);
    }

    show() {
        if (this.isDisabled) return;
        this.tooltip.classList.add('active');
        this.targetElement.classList.add(this.getPositionClassName());
    }

    hide() {
        this.tooltip.classList.remove('active');
    }

    disable() {
        this.hide();
        this.isDisabled = true;
    }

    enable() {
        this.isDisabled = false;
    }

}

class HoverTooltip extends Tooltip {

    constructor(targetSelector, text, placement = 'top') {
        super(targetSelector, text, placement);
        this.attachEvents();
    }

    attachEvents() {
        this.targetElement.addEventListener('mouseover', () => this.show());
        this.targetElement.addEventListener('mouseleave', () => this.hide());
    }
}

class ClicableTooltip extends Tooltip {

    constructor(targetSelector, text, placement = 'top') {
        super(targetSelector, text, placement);
        this.attachEvents();
    }

    toggle() {
        if (this.isDisabled) return;
        this.tooltip.classList.toggle('active');
        this.targetElement.classList.add(this.getPositionClassName());
    }

    attachEvents() {
        this.targetElement.addEventListener('click', this.toggle.bind(this));
    }
}

class FocusableTooltip extends Tooltip {

    constructor(targetSelector, text, placement = 'top', inputSelectorEl) {
        super(targetSelector, text, placement);
        this.inputSelectorEl = inputSelectorEl;
        this.attachEvents();
    }

    attachEvents() {
        this.inputSelectorEl.addEventListener("focus", () => this.show());
        this.inputSelectorEl.addEventListener("blur", () => this.hide());
    }
}



// кнопки с  подсказками
const firstTooltip = new HoverTooltip('#btn-1', 'It\'s first button');
const secondTooltip = new HoverTooltip('#btn-2', 'It\'s second button', 'right');
const thirdTooltip = new HoverTooltip('#btn-3', 'It\'s third button', 'bottom');
const fourthTooltip = new ClicableTooltip('#btn-4', 'It\'s fourth button', 'left');


// Кнопки с событиями
const showFirstBtnEl = document.querySelector('#show-first');
showFirstBtnEl.addEventListener('click', () => firstTooltip.show());

const hideFirstBtnEl = document.querySelector('#hide-first');
hideFirstBtnEl.addEventListener('click', () => firstTooltip.hide());

const disableThirdBtnEl = document.querySelector('#disable-third');
disableThirdBtnEl.addEventListener('click', () => thirdTooltip.disable());

const enableThirdBtnEl = document.querySelector('#enable-third');
enableThirdBtnEl.addEventListener('click', () => thirdTooltip.enable());

// Массив всех подсказок
const tooltips = [firstTooltip, secondTooltip, thirdTooltip, fourthTooltip]

const disableAllEl = document.querySelector('#disable-all');
disableAllEl.addEventListener('click', () => {
    tooltips.forEach(tooltip => tooltip.disable());
})

const enableAllEl = document.querySelector('#enable-all');
enableAllEl.addEventListener('click', () => {
    tooltips.forEach(tooltip => tooltip.enable());
})


// Поля с инпутами
const applyBtn = document.querySelector("#apply-text");
const applyInput = document.querySelector("#text-input");
applyBtn.addEventListener("click", () => {
    secondTooltip.changeText(applyInput.value);
    applyInput.value = "";

})

const applyBtn2 = document.querySelector("#apply-align");
const applyInput2 = document.querySelector("#align-input");
applyBtn2.addEventListener("click", () => {
    fourthTooltip.setPosition(applyInput2.value);
    applyInput2.value = "";
})

const inputEl = document.querySelector("#input input");
const inputTooltip = new FocusableTooltip('#input', 'It\'s input', 'bottom', inputEl);

